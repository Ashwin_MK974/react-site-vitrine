# React Site Vitrine


## Introduction
Projet en React JS d’un site web vitrine comportant plusieurs animations randomisées.



Ces animations sont en partie gérées par la librairie framer-motion afin de gérer les animations entre les pages.



Les pages utilisant des composants permettant la maintenabilité et la réutilisabilité dans le site Web.
## Visualisation
https://amk-agency.netlify.app/
## Installation

Pour installer les dépendances et lancer  le site:

```bash 
  git clone https://gitlab.com/Ashwin_MK974/react-site-vitrine.git
  cd react-site-vitrine
  npm install 
  npm start
```
    
![Logo](https://miro.medium.com/max/700/1*dLaDL-lSN0iprzmOpmM7zQ.png)

