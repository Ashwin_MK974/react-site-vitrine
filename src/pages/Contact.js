import { React, useEffect, useState } from "react";
import ContactForm from "../components/ContactForm";
import Logo from "../components/Logo";
import Mouse from "../components/Mouse";
import Navigation from "../components/Navigation";
import { CopyToClipboard } from "react-copy-to-clipboard";
import SocialNetwork from "../components/SocialNetwork";
import Buttons from "../components/Buttons";
import { motion } from "framer-motion";
const Contact = () => {
  const pageTransition = {
    out: {
      opacity: 0,
      x: 200,
    },
    in: {
      opacity: 1,
      x: 0,
      y: 0,
    },
  };
  useEffect(() => {
    const target = document.querySelectorAll(".content");
    const p = document.createElement("p");
    p.innerText = "Element copié ! ";
    p.className = "specifyCopy";
    target.forEach((i) => {
      i.addEventListener("click", function () {
        console.log(i.children.length);
        i.appendChild(p);
        setTimeout((_) => {
          if (i.children.length >= 2) {
            i.removeChild(p);
          }
        }, 2000);
      });
    });
  }, []);
  return (
    <main>
      <Mouse />
      <motion.div
        initial="out"
        animate="in"
        exit="out"
        className="contact"
        variants={pageTransition}
        transition={{ duration: 0.5 }}
      >
        <Navigation />
        <Logo />
        <ContactForm />
        <div className="contact-infos">
          <div className="address">
            <div className="content">
              <h4>adresse</h4>
              <p>12 rue laplace</p>
              <p>64200 Biarritz</p>
            </div>
          </div>
          <div className="phone">
            <div className="content">
              <h4>téléphone</h4>
              <CopyToClipboard text="0693473941">
                <p style={{ cursor: "pointer" }}>0693 47 39 41</p>
              </CopyToClipboard>
            </div>
          </div>
          <div className="email">
            <div className="content">
              <h4>email</h4>
              <CopyToClipboard text="a.malbrouck@rt-iut.re">
                <p style={{ cursor: "pointer" }}>a.malbrouck@rt-iut.re</p>
              </CopyToClipboard>
            </div>
          </div>
          <SocialNetwork />
          <div className="credits">
            <p>Ashwin MK - 2022</p>
          </div>
        </div>
        <Buttons left="/projet-4" />
      </motion.div>
    </main>
  );
};

export default Contact;
