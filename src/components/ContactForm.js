import { useEffect, useRef, React } from "react";
import emailjs from "@emailjs/browser";

export const ContactForm = () => {
  const form = useRef();
  let formMess;
  useEffect((_) => {
    formMess = document.querySelector(".form-message");
  }, []);

  const sendEmail = (e) => {
    e.preventDefault();

    emailjs
      .sendForm(
        "service_hhj661n",
        "template_cmgedtr",
        form.current,
        process.env.REACT_APP_ID
      )
      .then(
        (result) => {
          console.log(result.text);
          console.log(form.current.email.value);
          form.current.reset();
          formMess.innerHTML = " <p class='sucess'>Message envoyé ! </p> ";
          setTimeout((_) => {
            formMess.innerHTML = "";
          }, 3000);
        },
        (error) => {
          console.log(error.text);
          formMess.innerHTML =
            " <p class='fail'>Message non envoyé. Veuillez réessayer ! </p> ";
          setTimeout((_) => {
            formMess.innerHTML = "";
          }, 3000);
        }
      );
  };

  return (
    <div className="form-container">
      <form ref={form} onSubmit={sendEmail} className="form-content">
        <label>Nom</label>
        <input type="text" name="name" required autoComplete="off" id="name" />
        <label>Email</label>
        <input
          type="email"
          name="email"
          required
          autoComplete="off"
          id="email"
        />
        <label>Message</label>
        <textarea name="message" id="mess" />
        <input
          type="submit"
          value="Envoyer"
          required
          autoComplete="off"
          className="hover button"
        />
      </form>
      <div className="form-message"></div>
    </div>
  );
};
export default ContactForm;
