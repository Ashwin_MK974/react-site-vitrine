import { React, useEffect } from "react";

const DynamicText = () => {
  useEffect((_) => {
    const target = document.getElementById("text-target");
    let array = ["simple", "clear", "smart", "strong"];
    let wordIndex = 0;
    let letterIndex = 0;

    const createLetter = () => {
      const letter = document.createElement("span");
      target.appendChild(letter);

      letter.classList.add("letter");
      letter.style.oppacity = 0;
      letter.style.animation = "anim 5s ease forwards";
      letter.textContent = array[wordIndex][letterIndex];

      setTimeout(() => {
        letter.remove();
      }, 2000);
    };
    //s
    const loop = () => {
      setTimeout(() => {
        if (letterIndex < array[wordIndex].length) {
          createLetter();
          letterIndex++;
          loop();
        } else {
          createLetter();
          letterIndex = 0;
          if (wordIndex + 1 >= array.length) {
            //console.log("overflow ...");
            wordIndex = 0;
          } else {
            wordIndex++;
            //console.log("wordIndex : ", wordIndex);
          }

          setTimeout(() => {
            loop(); //avant de faire l'animation sure le mot suivant on définit un setTimeout
          }, 2000);
        }
      }, 80);
    };

    loop();
  }, []);
  return (
    <span className="dynamic-text">
      <span>simply</span>
      <span id="text-target"></span>
    </span>
  );
};

export default DynamicText;
